export const state = () => ({
    hotels: [],
    numberOfNights: "1",
    idToShow: 0,
    details: {},
    reviews: [],
    pageNumber: 1,
    pages: 1,
    range: {
      from: 0,
      to: 3
    }
  })
  
  export const mutations = {
    setHotels(state, value) {
      state.hotels = value;
    },
    updateNumberOfNights(state, value) {
      state.numberOfNights = value;
    },
    updateId(state, value) {
      state.idToShow = value;
    },
    updateDetails(state, value){
      state.details = value;
    },
    setReviews(state, value){
      state.reviews = value;
    },
    updateRange(state, value){
      state.range = value;
    },
    changePage(state, value){
      state.pageNumber = value;
    },
    setPages(state, value){
      state.pages = value;
    }
  }