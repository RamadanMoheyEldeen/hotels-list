import axios from 'axios';


export const http = {
  methods: {
    callService(method, url){
        const headers = {
            'Accept': "application/json",
            'Content-Type': 'application/json'
        }
        return axios({
            url: `http://my-json-server.typicode.com/fly365com/code-challenge${url}`,
            method,
            headers
        })
    },
    services(){
        return {
            hotels: {
                getDetails: (id) => this.callService('GET', `/hotelDetails/${id}`),
                getAll: () => this.callService('GET', '/hotels'),
            }
        }
    }
  }
}