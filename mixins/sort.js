export const sort = {
    methods:{
        sort(type, items) {
            if (type == "descending")
              return items.map(item => item).sort((a, b) => (a.score < b.score ? 1 : -1));
            else 
              return items.map(item => item).sort((a, b) => (a.score > b.score ? 1 : -1));
          }
    }
}