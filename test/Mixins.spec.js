import { mount } from '@vue/test-utils'
import { sort } from '@/mixins/sort'

describe('sort', () => {
  test('Test sort function descending', () => {
    const Component = {
      render() {},
      mixins: [sort]
    }
    const wrapper = mount(Component)
    const reviews = [{score: 10, review: "abc"},
                     {score: 8, review: "abc"},
                     {score: 15, review: "abc"}]
    const results = [{score: 15, review: "abc"},
                     {score: 10, review: "abc"},
                     {score: 8, review: "abc"}]
    expect(wrapper.vm.sort('descending', reviews)).toStrictEqual(results)
  })
})

