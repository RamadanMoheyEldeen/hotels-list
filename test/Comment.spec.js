import { mount } from '@vue/test-utils'
import Comment from '@/components/Comment.vue'

describe('Comment', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(Comment)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  test('renders correctly', () => {
    const wrapper = mount(Comment)
    expect(wrapper.element).toMatchSnapshot()
  })
})
