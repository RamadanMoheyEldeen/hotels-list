import { mount , shallowMount } from '@vue/test-utils'
import HotelCard from '@/components/HotelCard.vue'

describe('HotelCard', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(HotelCard)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  test('renders HotelCard correctly', () => {
    const wrapper = mount(HotelCard)
    expect(wrapper.element).toMatchSnapshot()
  })

  it('renders the correct message for review', () => {
    const wrapper = mount(HotelCard)
    expect(wrapper.vm.formatScore(9)).toBe('9 Excellent')
  })

  it('renders the correct message for review', () => {
    const wrapper = mount(HotelCard)
    expect(wrapper.vm.formatScore(1)).toBe('1 ----')
  })
  
})
